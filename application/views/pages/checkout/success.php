<div class="container">
	<div class="row mt-4">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Checkout Berhail</h5>	
				<div class="card-body">
					<h4><strong>Order Number : <?= $content['invoice'] ?></strong></h4>
					<p>Thank You For Shopping.</p>
					<br>
					<p>Silahkan Melakukan Pembayaran Dengan Prosedur Sebagai Berikut :</p>
					<ol>
						<li>Lakukan Pembayaran Ke Rekening BCA <strong>BCA 0123456789</strong> An. Soklat Banget </li>
						<li>Masukan Informasi Dengan Nomor Pesanan <strong><?= $content['invoice'] ?></strong></li>
						<li>Total Pembayaran <strong>Rp. <?= number_format($content['total'], 0, ',', '.') ?></strong></li>
					</ol>
					<p>Jika Sudah Melakukan Pembayaran, Mohon Kirimkan Bukti Transfer<a href="<?= base_url('myorder/detail/' . $content['invoice']) ?>"> Klik Disini</a></p>
					<a href="<?= base_url('home') ?>" class="btn btn-primary btn-sm">Back</a>
				</div>
			</div>
		</div>
	</div>
</div>
